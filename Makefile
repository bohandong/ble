CC = gcc
CFLAGS =
BTFLAGS = -lbluetooth

all: bt_test

bt_test: bt_test.o mc_log.o
	$(CC) $(CFLAGS) -o bt_test bt_test.o mc_log.o $(BTFLAGS)

bt_test.o: bt_test.c mc_log.h
	$(CC) $(CFLAGS) -c bt_test.c $(BTFLAGS)

mc_log.o: mc_log.c mc_log.h
	$(CC) $(CFLAGS) -c mc_log.c

clean:
	@rm -f *.o bt_test bt_test.log
