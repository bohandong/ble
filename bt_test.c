#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

#include "mc_log.h"

#define MAX_SCAN 10 	//SUGGEST "MAX_SCAN 255"
#define LEN 4			//SEARCH LASTS FOR 4*1.25 = 5S
#define HCIDEVICEID 0	//DEFAULT ADAPTER ID. IF < 0, SELECT FIRST AVAILABLE ADAPTER
#define ATT_CID 4
#define BDADDR_LE_PUBLIC 0x01

struct sockaddr_l2 {
	sa_family_t		l2_family;
	unsigned short	l2_psm;
	bdaddr_t		l2_bdaddr;
	unsigned short	l2_cid;
	uint8_t			l2_bdaddr_type;
};

inquiry_info *devices = NULL;
char UUID_Service[33] = "12345678901234567890123456789012";
char NAME[6] = "Vinet";

/*
int dsb_scan(const int adapter_id)
{
	int num, sock, flags, i;
	char addr[19] = {0};
	char name[248] = {0};

	//OPEN SOCKET
	sock = hci_open_dev(adapter_id);
	if (sock < 0)
	{
		printf("ERROR[DSBE# BT002]: Cannot open socket!\n");
		return -1;
	}

	//INITIALIZATION & MEMORY ALLOCATION
	printf("Scanning on socket %d...\n", sock);
	flags = IREQ_CACHE_FLUSH;
	devices = (inquiry_info *)malloc(MAX_SCAN * sizeof(inquiry_info));

	//INQUIRE THE NUMBER OF RESPONDING DEVICES
	num = hci_inquiry(adapter_id, LEN, MAX_SCAN, NULL, &devices, flags);
	if (num < 0)
	{
		printf("ERROR[DSBE# BT003]: HCI inquiry failed!\n");
		close(sock);
		return -1;
	}
	else printf("Found %d devices!\n", num);

	//PRINT NAME & ADDRESS OF EACH VISIBLE DEVICE
	for (i = 0; i < num; i++)
	{
		ba2str(&(devices + i)->bdaddr, addr);		//TURN ADDRESS OF DEVICE INTO STRING
		memset(name, 0, sizeof(name));
		if (0 != hci_read_remote_name(sock, &(devices + i)->bdaddr, sizeof(name), name, 0))
		{
			strcpy(name, "[UNKNOWN]");			//IF THERE IS NO NAME FOUND, PRINT "[UNKNOWN]"
		}
		printf("#%i.\t%s\t%s\n", i, name, addr);
	}

	close(sock);
	return num;
}

void dsb_client(char device_addr[])
{
	struct sockaddr_rc addr = {0};
	int sock, status;

	//NEXUS6 F8:CF:C5:A6:1E:29
	//char dest[18] = "F8:CF:C5:A6:1E:29";
	printf("The BDAddr you selected is %s\n", device_addr);

	//ALLOCATE A SOCKET
	sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

	//SET THE CONNECTION PARAMETERS (WHO TO CONNECT TO)
	addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = (uint8_t) 0;
	str2ba(device_addr, &addr.rc_bdaddr);

	printf("test1\n");

	//CONNECT TO SERVER
	status = connect(sock, (struct sockaddr*)&addr, sizeof(addr));

	//SEND A MESSAGE
	if (status == 0)
	{
		status = write(sock, "Hello World!", 6);
	}
	else printf("ERROR[DSBE# BT005]: Cannot connect to the device!\n");
	if (status < 0) printf("ERROR[DSBE# BT004]: Cannot send message!\n");

	close(sock);
}

void dsb_server()
{
	struct sockaddr_rc local_addr = {0}, remote_addr = {0};
	char buf[1024] = {0};
	char end[] = "end";
	int sock, client, bytes_read;
	socklen_t opt = sizeof(remote_addr);

	//ALLOCATE SOCKET
	sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

	//BIND SOCKET TO PORT 0/1 OF THE FIRST AVAILABLE LOCAL BLUETOOTH ADAPTER
	local_addr.rc_family = AF_BLUETOOTH;
	local_addr.rc_bdaddr = *BDADDR_ANY;
	local_addr.rc_channel = (uint8_t) 1;
	bind(sock, (struct sockaddr*)&local_addr, sizeof(local_addr));


	fd_set rfds;
	struct timeval tv;
	int selectRetval, ad;
	FD_ZERO(&rfds);
	FD_SET(0, &rfds);
	FD_SET(sock, &rfds);
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	selectRetval = select(sock + 1, &rfds, NULL, NULL, &tv);
	printf("socket: %d; selectRetval: %d\n", sock, selectRetval);
	ad = hci_le_set_advertise_enable(sock, 1, 1000);
	printf("ad = %d\n", ad);

	//PUT SOCKET INTO LISTENING MODE
	listen(sock, 1);
	printf("Listening...\n");

	//ACCEPT ONE CONNECTION
	client = accept(sock, (struct sockaddr*)&remote_addr, &opt);
	ba2str(&remote_addr.rc_bdaddr, buf);
	printf("Accept connection from %s\nTry to type something:\n", buf);

	//READ DATA FROM THE CLIENT
	do {
		memset(buf, 0, sizeof(buf));
		bytes_read = recv(client, buf, sizeof(buf), 0);
		if (bytes_read > 0)
		{
			send(client, "Got it!\n", sizeof("Got it!\n"), 0);
			printf("received [%s]\n", buf);
		}
	}
	while (strcmp(buf, end));

	close(client);
	close(sock);
}
*/
int hci_le_set_scan_response_data(int sock, uint8_t* data, uint8_t length, int to)
{
	struct hci_request rq;
	le_set_scan_response_data_cp data_cp;
	uint8_t status;

	memset(&data_cp, 0, sizeof(data_cp));
	data_cp.length = length <= sizeof(data_cp.data) ? length : sizeof(data_cp.data);
	memcpy(&data_cp.data, data, data_cp.length);

	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = OCF_LE_SET_SCAN_RESPONSE_DATA;
	rq.cparam = &data_cp;
	rq.clen = LE_SET_SCAN_RESPONSE_DATA_CP_SIZE;
	rq.rparam = &status;
	rq.rlen = 1;

	if (hci_send_req(sock, &rq, to) < 0) {
		return -1;
	}

	if (status) {
		errno = EIO;
		return -1;
	}

	return 0;
}

int hci_le_set_advertising_data(int sock, uint8_t* data, uint8_t length, int to)
{
	struct hci_request rq;
	le_set_advertising_data_cp data_cp;
	uint8_t status;

	memset(&data_cp, 0, sizeof(data_cp));
	data_cp.length = length <= sizeof(data_cp.data) ? length : sizeof(data_cp.data);
	memcpy(&data_cp.data, data, data_cp.length);

	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = OCF_LE_SET_ADVERTISING_DATA;
	rq.cparam = &data_cp;
	rq.clen = LE_SET_ADVERTISING_DATA_CP_SIZE;
	rq.rparam = &status;
	rq.rlen = 1;

	if (hci_send_req(sock, &rq, to) < 0) {
		return -1;
	}

	if (status) {
		errno = EIO;
		return -1;
	}

	return 0;
}

// Set advertising interval to 100 ms
// Note: 0x00A0 * 0.625ms = 100ms
int hci_le_set_advertising_parameters(int sock, int to)
{
	struct hci_request rq;
	le_set_advertising_parameters_cp adv_params_cp;
	uint8_t status;

	memset(&adv_params_cp, 0, sizeof(adv_params_cp));
	adv_params_cp.min_interval = htobs(0x00A0);
	adv_params_cp.max_interval = htobs(0x00A0);
	adv_params_cp.chan_map = 7;

	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = OCF_LE_SET_ADVERTISING_PARAMETERS;
	rq.cparam = &adv_params_cp;
	rq.clen = LE_SET_ADVERTISING_PARAMETERS_CP_SIZE;
	rq.rparam = &status;
	rq.rlen = 1;

	if (hci_send_req(sock, &rq, to) < 0) {
		return -1;
	}

	if (status) {
		errno = EIO;
		return -1;
	}

	return 0;
}

int main(int argc, char const *argv[])
{
	log_init(LOG_PVERB, "bt_test.log");
	log_debug(LOG_DEBUG, "\n\n\n\n\nStart!");


	//Setup Adapter ID
	int adapter_id = -1;

	adapter_id = (HCIDEVICEID < 0) ? (hci_get_route(NULL)) : HCIDEVICEID;
	if (adapter_id < 0)
	{
		log_error("ERROR: Adapter is not available!");
		exit(1);
	}

	//Setup HCI Socket
	int hci_socket = -1;

	hci_socket = hci_open_dev(adapter_id);
	if (hci_socket < 0) {
		log_error("ERROR: Adapter state is unsupported!");
		exit(1);
	}


	//Setup tv
	struct timeval tv;


	//Start 2 Process
	pid_t pid = fork();

	if (pid == 0) {
		//Child Process: L2cap

		//Setup L2cap Socket
		int l2cap_socket = -1;
		bdaddr_t daddr;

		l2cap_socket = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
		if (hci_read_bd_addr(hci_socket, &daddr, 1000) == -1) {
			daddr = *BDADDR_ANY;
		}

		//Bind & Listen
		struct sockaddr_l2 l2cap_socketAddr;
		int result = -1;

		memset(&l2cap_socketAddr, 0, sizeof(l2cap_socketAddr));
		l2cap_socketAddr.l2_family = AF_BLUETOOTH;
		l2cap_socketAddr.l2_bdaddr = daddr;
		l2cap_socketAddr.l2_cid = htobs(ATT_CID);
		l2cap_socketAddr.l2_bdaddr_type = BDADDR_LE_PUBLIC;

		result = bind(l2cap_socket, (struct sockaddr*)&l2cap_socketAddr, sizeof(l2cap_socketAddr));
		log_debug(LOG_DEBUG, "L2cap Bind %s", (result == -1) ? strerror(errno) : "Success");

		result = listen(l2cap_socket, 1);
		log_debug(LOG_DEBUG, "L2cap Listen %s", (result == -1) ? strerror(errno) : "Success");


		fd_set afds;

		while (result != -1) {
			FD_ZERO(&afds);
			FD_SET(l2cap_socket, &afds);

			tv.tv_sec = 2;
			tv.tv_usec = 0;

			result = select(l2cap_socket + 1, &afds, NULL, NULL, &tv);
			log_debug(LOG_DEBUG, "L2cap Select = %d", result);
		}
	}
	else if (pid > 0) {
		//Parent Process: HCI

		struct hci_dev_info adapter_info;

		memset(&adapter_info, 0x00, sizeof(adapter_info));
		adapter_info.dev_id = adapter_id;

		fd_set rfds;
		int selectRetval;
		char * adapterState;
		char adapter_addr[18];
		int curAdapterState;
		int preAdapterState = -1;
		char advData[256], scanData[256];
		int advDataLen, scanDataLen;
		char advBuf[43] = "020105110612907856341290785634129078563412";
		char scanBuf[41] = "1308496e74726f20746f20526173706920424c45";
		//char scanBuf[41] = "060856494e4554";							//sizeof(VINET)+1, 0x08, VINET

		while (1) {
			FD_ZERO(&rfds);
			FD_SET(0, &rfds);
			FD_SET(hci_socket, &rfds);

			tv.tv_sec = 2;
			tv.tv_usec = 0;

			ioctl(hci_socket, HCIGETDEVINFO, (void *) &adapter_info);
			curAdapterState = hci_test_bit(HCI_UP, &adapter_info.flags);
			ba2str(&adapter_info.bdaddr, adapter_addr);

			if (preAdapterState != curAdapterState) {
				preAdapterState = curAdapterState;

				if (!curAdapterState) {
					adapterState = "POWER OFF";
				}
				else {
					// hci_le_set_advertise_enable(hci_socket, 0, 1000);
					// hci_le_set_advertise_enable(hci_socket, 1, 1000);

					// if (hci_le_set_advertise_enable(hci_socket, 0, 1000) == -1) {
					// 	if (EPERM == errno) {
					// 		adapterState = "UNAUTHORIZED";
					// 	}
					// 	else if (EIO == errno) {
					// 		adapterState = "UNSUPPORTED";
					// 	}
					// 	else {
					// 		log_error("ERROR: Unknown Adapter Error: %d", errno);
					// 		adapterState = "UNKNOWN";
					// 	}

					// }
					// else {
					// 	adapterState = "POWER ON";
					// }

					adapterState = "POWER ON";
				}

				log_debug(LOG_DEBUG, "HCI%d [%s] %s", adapter_id, adapterState, adapter_addr);
			}

			selectRetval = select(hci_socket + 1, &rfds, NULL, NULL, &tv);
			log_debug(LOG_DEBUG, "HCI Select = %d", selectRetval);

			if (selectRetval == 1) {
				if (FD_ISSET(0, &rfds)) {
					unsigned int data;
					int i;

					advDataLen = 0;
					for (i = 0; advBuf[i] != '\0'; i += 2) {
						data = 0;
						sscanf(&advBuf[i], "%02x", &data);
						advData[advDataLen] = data;
						advDataLen++;
					}

					scanDataLen = 0;
					for (i = 0; scanBuf[i] != '\0'; i += 2) {
						data = 0;
						sscanf(&scanBuf[i], "%02x", &data);
						scanData[scanDataLen] = data;
						scanDataLen++;
					}

					log_debug(LOG_DEBUG, "advData: %s\t scanData: %s", advData, scanData);



					hci_le_set_advertise_enable(hci_socket, 0, 1000);

					hci_le_set_scan_response_data(hci_socket, (uint8_t*)&scanData, scanDataLen, 1000);

					hci_le_set_advertising_data(hci_socket, (uint8_t*)&advData, advDataLen, 1000);

					hci_le_set_advertising_parameters(hci_socket, 1000);

					hci_le_set_advertise_enable(hci_socket, 1, 1000);

					hci_le_set_scan_response_data(hci_socket, (uint8_t*)&scanData, scanDataLen, 1000);

					hci_le_set_advertising_data(hci_socket, (uint8_t*)&advData, advDataLen, 1000);
				}
			}
		}
	}
	else {
		//Fork Failed
		printf("Fork Failed\n");

		log_error("ERROR: Fork failed!");
		exit(1);
	}

	hci_le_set_advertise_enable(hci_socket, 0, 1000);

	close(hci_socket);

	return 0;
}